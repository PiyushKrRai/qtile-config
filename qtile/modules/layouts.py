from libqtile import layout
from libqtile.config import Match

bg0="#282828"
bg1="#3c3836"
bg2="#504945"
bg3="#665c54"
bg4="#7c6f64"
fg0="#fbf1c7"
fg1="#ebdbb2"
fg2="#d5c4a1"
fg3="#bdae93"
fg4="#a89984"

##og
##border_focus='#5294e2',border_normal='#2c5380'

layouts = [
    layout.MonadTall(margin=5, border_focus=fg2, border_normal=bg2),
    layout.Max(),
    layout.Matrix(margin=2, border_focus=fg2, border_normal=bg2),
    layout.TreeTab(active_bg='#ebdbb2', active_fg='#000000', bg_color='1d2021', inactive_bg='#3c3836', place_right='true',),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    #layout.Columns(border_focus_stack='#d75f5f'),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

floating_layout = layout.Floating(border_focus=fg2, border_normal=bg2,float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(title='Open File'),
])
