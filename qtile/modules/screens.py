from libqtile import bar
from .widgets import *
from libqtile.config import Screen
from modules.keys import terminal
import os

bg0="#282828"
bg1="#3c3836"
bg2="#504945"
bg3="#665c54"
bg4="#7c6f64"
fg0="#fbf1c7"
fg1="#ebdbb2"
fg2="#d5c4a1"
fg3="#bdae93"
fg4="#a89984"

screens = [
    Screen(
        top=bar.Bar(
            [   widget.Sep(padding=3, linewidth=0, background=bg0),
                widget.Image(filename='~/.config/qtile/arch.png', margin=3, background=bg0, mouse_callbacks={'Button1': lambda: qtile.cmd_spawn("bash /home/piyush/.config/rofi/launch.sh")}),
                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg0, background=bg1,padding = 0,fontsize =30),

                widget.GroupBox(
                                highlight_method='line',
                                highlight_color=bg0,
                                this_screen_border=fg4,
                                this_current_screen_border=fg1,
                                #block_highlight_text_color=bg1,
                                active="#ffffff",
                                inactive=bg4,
                                urgent_border="#cc241d",
                                #background="#2f343f"
                                background=bg1,
                                ),
                
                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg1,background=bg3,padding = 0,fontsize = 30),
                
                widget.CurrentLayoutIcon(scale=0.75,background=bg3),

                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg3,background=bg2,padding = 0,fontsize = 30),

                widget.Prompt(),

                ##SYS Monitors 
                widget.TextBox(text='',foreground=fg1,background=bg2),
                widget.ThermalSensor(format='{temp:.0f}{unit}',foreground=fg1,background=bg2),

                widget.TextBox(text='|',foreground=fg1,background=bg2),
                widget.TextBox(text='',foreground=fg1,background=bg2),
                widget.CPU(
                    format='{load_percent}%',foreground=fg1,background=bg2
                    ),
                
                widget.TextBox(text='|',foreground=fg1,background=bg2),
                widget.TextBox(text='',foreground=fg1,background=bg2),
                widget.Memory(
                    measure_mem='G',foreground=fg1,background=bg2),

                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg2,background=bg3,padding = 0,fontsize = 30),

                widget.CheckUpdates(
                    update_interval=900,
                    distro="Arch_checkupdates",
                    display_format="{updates} ",
                    foreground=fg1,
                    background=bg3,
                    padding= 5,
                    no_update_string='',
                    mouse_callbacks={
                        'Button1':
                        lambda: qtile.cmd_spawn(terminal + ' -e yay -Syu')
                    },
                    ),

                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg3,background=bg0,padding = 0,fontsize = 30),






                widget.Spacer(background=bg0),

                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg3,background=bg0,padding = 0,fontsize = 30),
                widget.Sep(padding=3, linewidth=0, background=bg3),
                widget.Clock(format=' %I:%M %p | %d/%m/%Y %a', padding= 5,
                             background=bg3,
                             foreground=fg1),
                widget.Sep(padding=3, linewidth=0, background=bg3),
                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg3,background=bg0,padding = 0,fontsize = 30),
                
                widget.Spacer(background=bg0),
           




                #widget.WindowCount(foreground=fg0,background=bg0,fmt='{} |'),
                #widget.WindowName(foreground=fg0,background=bg0,fmt='{}',max_chars=50),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),



                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg1,background=bg0,padding = 0,fontsize = 30),

                widget.Net(
                    background=bg1,foreground=fg0, format= ' {down}'),
                widget.Net(
                    background=bg1,foreground=fg4, format= ' {up}'),

                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg4,background=bg1,padding = 0,fontsize = 30),

                #widget.OpenWeather(location='Kolkata', format=' {main_temp} °{units_temperature} |  {humidity}% |  {weather_details}',foreground=fg1,background=bg4),
                widget.OpenWeather(location='Kolkata', 
                                format=' {main_temp} °{units_temperature}',
                                foreground=fg1,background=bg4,
                                max_chars=15,
                                scroll=True,
                                scroll_delay=0,
                                scroll_interval=1,
                                ),
                          
                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg2,background=bg4,padding = 0,fontsize = 30),
                widget.Battery(
                    format='{char} {percent:2.0%}',
                    charge_char='', discharge_char='', full_char='',
                    background=bg2,
                    foreground=fg1
                    ),

                widget.TextBox(text='|',font = "Ubuntu Mono",foreground=fg1,background=bg2), # S P A C E R

                widget.Backlight(brightness_file='/sys/class/backlight/intel_backlight/brightness',
                                max_brightness_file='/sys/class/backlight/intel_backlight/max_brightness',
                                fmt=' {}',background=bg2,foreground=fg1),

                widget.TextBox(text='|',font = "Ubuntu Mono",foreground=fg1,background=bg2), # S P A C E R

                widget.Volume(fmt=' {}',background=bg2,foreground=fg1,
                    mouse_callbacks ={'Button1': lambda: qtile.cmd_spawn("pavucontrol")}
                    ),

                ##widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg3,background=bg2,padding = 0,fontsize = 30),
                
                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg1,background=bg2,padding = 0,fontsize = 30),

                widget.Systray(icon_size = 20, padding= 5,background=bg1,foreground=fg1),
                #volume,
                
                widget.TextBox(text='',font = "Ubuntu Mono",foreground=bg0,background=bg1,padding = 0,fontsize = 30),

                widget.TextBox(
                    text='', padding= 5,background=bg0,
                    mouse_callbacks= {
                        'Button1':
                        lambda: qtile.cmd_spawn(os.path.expanduser('~/.config/rofi/powermenu.sh'))
                    },
                    foreground='#e39378'
                ),
                widget.Sep(padding=3, linewidth=0, background=bg0),
                
            ],
            30,  # height in px
            background="#282828",  # background color
            opacity=0.75
        ), ),
]


##widget.TextBox(text='',font = "Ubuntu Mono",foreground='#2f343f',padding = 0,fontsize = 25),
                ##widget.TextBox(text='',font = "Ubuntu Mono",background='#2f343f',foreground='#404552',padding = 0,fontsize = 25),
## 
## 