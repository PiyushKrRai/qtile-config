from libqtile import hook
import subprocess
import os

#@hook.subscribe.startup_once
@hook.subscribe.startup
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])
