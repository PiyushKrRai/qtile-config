#!/bin/sh

#killall volumeicon
#picom & disown # --experimental-backends --vsync should prevent screen tearing on most setups if needed
# Low battery notifier
#~/.config/qtile/scripts/check_battery.sh & disown
# Start welcome
#eos-welcome & disown
#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & disown # start polkit agent from GNOME
#volumeicon & disown
#nm-applet & disown

#Wallpaper
feh --bg-scale --randomize "$HOME"/Pictures/wallpapers/ &

#PICOM
picom --experimental-backends -CGb --vsync &

#EOS WELCOME
eos-welcome &

#System Tray icons
## (sleep30; volumeicon) &
nm-applet --indicator &

# Gnome Polkit
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

#Low Battery
~/.config/qtile/scripts/check_battery.sh &
